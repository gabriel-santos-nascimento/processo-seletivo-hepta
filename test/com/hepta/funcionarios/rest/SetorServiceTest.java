package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import com.google.protobuf.Service;

import com.hepta.funcionarios.entity.Setor;

class SetorServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("Testes Junit Funcionario");
	}
	
	
	@Test
	void testSetorRead() {
		SetorService sr = new SetorService();
	    
		 Response resposta = sr.SetorRead();
		 
		 System.out.println(resposta);
		
		
	} 
	@Test
	void testSetorListaUmFuncionario(@PathParam("id") Integer id) {
		SetorService fr = new SetorService();
	    
		 Response resposta = fr.ListaUmSetor(id);
		 
		 System.out.println(resposta);
		
		
	} 

	@Test
	void testSetorCreate(Setor Setor) {
		
		SetorService sr = new SetorService();
	    
		Response resposta = sr.SetorCreate(Setor);
		 
		System.out.println(resposta);
		
	
	}
	
	

	@Test
	void testSetorUpdate(@PathParam("id") Integer id, Setor Setor) {
		
		SetorService sr = new SetorService();
	    
		Response resposta = sr.SetorUpdate(1,Setor); // Exemplo com o id 1
		 
		System.out.println(resposta);
	}

	@Test
	void testFuncionarioDelete(@PathParam("id") Integer id) {
	
		SetorService fr = new SetorService();
	    
		Response resposta = fr.SetorDelete(2); // Exemplo com o id 2
		 
		System.out.println(resposta);
	}

}

