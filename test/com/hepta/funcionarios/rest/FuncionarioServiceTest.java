package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import com.google.protobuf.Service;
import com.hepta.funcionarios.entity.Funcionario;


class FuncionarioServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("Testes Junit Funcionario");
	}
	
	
	@Test
	void testFuncionarioRead() {
	     FuncionarioService fr = new FuncionarioService();
	    
		 Response resposta = fr.FuncionarioRead();
		 
		 System.out.println(resposta);
		
		
	} 
	@Test
	void testFuncionarioListaUmFuncionario(@PathParam("id") Integer id) {
	     FuncionarioService fr = new FuncionarioService();
	    
		 Response resposta = fr.ListaUmFuncionario(id);
		 
		 System.out.println(resposta);
		
		
	} 

	@Test
	void testFuncionarioCreate(Funcionario Funcionario) {
		
		FuncionarioService fr = new FuncionarioService();
	    
		Response resposta = fr.FuncionarioCreate(Funcionario);
		 
		System.out.println(resposta);
		
	
	}
	
	

	@Test
	void testFuncionarioUpdate(@PathParam("id") Integer id, Funcionario Funcionario) {
		
		FuncionarioService fr = new FuncionarioService();
	    
		Response resposta = fr.FuncionarioUpdate(1,Funcionario); // Exemplo com o id 1
		 
		System.out.println(resposta);
	}

	@Test
	void testFuncionarioDelete(@PathParam("id") Integer id) {
	
		FuncionarioService fr = new FuncionarioService();
	    
		Response resposta = fr.FuncionarioDelete(2); // Exemplo com o id 2
		 
		System.out.println(resposta);
	}

}
