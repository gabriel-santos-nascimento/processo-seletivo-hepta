package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;


@Path("/setor")
public class SetorService {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private SetorDAO dao;

	public SetorService() {
		dao = new SetorDAO();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}


	 // Adiciona novo Setor
	
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response SetorCreate(Setor Setor) {
		try {
			dao.save(Setor);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar Setor").build();

				}
		
		return Response.status(Status.OK).entity("Setor Criado").build();
	}

	
	// Lista todos os Setores
	

	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response SetorRead() {
		List<Setor> Setor = new ArrayList<>();
		try {
			Setor = dao.getAll();
		
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setores").build();
		}

		GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(Setor) {
		};
		return Response.status(Status.OK).entity(entity+"\n Setores listados ").build();
	}
	
	
	 // Lista um Setor
	 
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response ListaUmSetor(@PathParam("id") Integer id){
		Setor st;
		try {
			st = dao.find(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setor").build();
			
				}
		GenericEntity<Setor> entity = new GenericEntity<Setor>(st) {
		};
		return Response.status(Status.OK).entity(entity + "\n Setor Listado").build();
		
	}
			

	
	 // Atualiza um Setor
	
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response SetorUpdate(@PathParam("id") Integer id, Setor Setor) {
		
		try {
			 dao.update(Setor);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar Setor").build();

				}
		return Response.status(Status.OK).entity("Setor Atualizado").build();
	}

	// Remove um Setor
	
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response SetorDelete(@PathParam("id") Integer id) {
		
		try {
			 dao.delete(id);
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar Setor").build();

				}
		return Response.status(Status.OK).entity("Funcionário Deletado").build();
	}
}
